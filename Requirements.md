Requirements for running giantbomb.go

This is a standard go program.  Set up a go environment as detailed at
golang.org.  Then, place giantbob.go under ~/Go/src/giantbomb.  cd to that
directory and run 'go run giantbomb'

You will need of course to first edit giantbomb.go and set api_key to your
actual giantbomb api key.
