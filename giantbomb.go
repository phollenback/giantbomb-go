// giantbomb - query the giantbomb.com api for some data and display it.
// philiph@pobox.com
// 6/24/19

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"encoding/json"
	"strconv"
	"os"
)

// put your actual api key here
var api_key = "XXXXXXXXXX"
// take two - looked up on the giantbomb website
var company_guid="3010-450"
var url_prefix="https://www.giantbomb.com/api"

// json returned for the publisher, includes a list of published games
type Publisher struct {
	Results                 struct {
		PublishedGames []struct {
			ID              int     `json:"id"`
			Name            string  `json:"name"`
		} `json:"published_games"`
	}       `json:"results"`
}

// json returned for each published game, includes a list of reviews
type Game struct {
	Results                 struct {
		ExpectedReleaseDay      int     `json:"expected_release_day"`
		ExpectedReleaseMonth    int     `json:"expected_release_month"`
		ExpectedReleaseYear     int     `json:"expected_release_year"`
		Name                    string  `json:"name"`
		Reviews                 []struct {
			ID              int     `json:"id"`
			Name            string  `json:"name"`
		}       `json:"reviews"`
	}       `json:"results"`
}

// json returned for each review
type Review struct {
	Results                 struct {
		Reviewer        string  `json:"reviewer"`
		Score           int     `json:"score"`
		Dlc             bool    `json:"Dlc"`
	}       `json:"results"`
}

// Individual game/review results that I construct
type Result struct {
	Title       string
	Reviewer    string
	ReleaseDate string
	IsDlc       bool
}

// final output json.  It needs to match the below sample output.
type Output struct {
	LabelName struct {
		ReviewsByScore struct {
			Key5    []Result   `json:"5"`
			Key4    []Result   `json:"4"`
			Key3    []Result   `json:"3"`
			Key2    []Result   `json:"2"`
			Key1    []Result   `json:"1"`
		} `json:"ReviewsByScore"`
	} `json:"Take-Two Interactive"`
}

/* Here's what the output is supposed to look like:

{
  "$LabelName": {
    "ReviewsByScore": [
      "5": [ {
	"Title": "Title Name",
	"Reviewer": "Reviewer Name",
	"ReleaseDate": "01/01/2019"
	}
      ],
      "4": [
	{
	  "Title": "Title Name",
	  "Reviewer": "Reviewer Name",
	  "ReleaseDate": "01/01/2019",
	  "IsDlc": true
	}
      ]
    ]
  }
}
*/

// get the list of game ids for games pubished by Take Two
func getGameIds() []int {

	game_ids := []int{}
	var publisher Publisher

	response, err := http.Get(url_prefix + "/company/" + company_guid + "/?api_key=" + api_key +
		"&format=json&field_list=published_games")
	if err != nil {
		fmt.Fprintf(os.Stderr, "http request failed: %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		if err := json.Unmarshal(data, &publisher); err != nil {
			fmt.Fprintf(os.Stderr, "json unmarshal failed: %s\n", err)
		}
	}
	// create a 1d array of game ids for all games from the publisher
	for i := 0; i < len(publisher.Results.PublishedGames); i++ {
		game_ids = append(game_ids, publisher.Results.PublishedGames[i].ID)
	}

	return game_ids
}

// get the game data for a list of game ids.
func getGames(game_ids []int) []Game {

	var games []Game

	for _, game_id := range game_ids {
		response, err := http.Get(url_prefix + "/game/3030-" + strconv.Itoa(game_id) + "/?api_key=" + api_key +
			"&format=json&field_list=name,expected_release_day,expected_release_month,expected_release_year,reviews")
		if err != nil {
			fmt.Fprintf(os.Stderr, "http request failed: %s\n", err)
		} else {
			data, _ := ioutil.ReadAll(response.Body)
			var game Game
			if err := json.Unmarshal(data, &game); err != nil {
				fmt.Fprintf(os.Stderr,"json unmarshal failed: %s\n", err)
			} else {
				games = append(games, game)
			}
		}
	}
	return games
}

// get the reviews for a particular game
func getReviews(game Game) []Review {
	var reviews []Review

	for i:=0; i<len(game.Results.Reviews); i++ {
		response, err := http.Get(url_prefix + "/review/1900-" + strconv.Itoa(game.Results.Reviews[i].ID) +
			"/?api_key=" + api_key + "&format=json&field_list=reviewer,score,dlc")
		if err != nil {
			fmt.Fprintf(os.Stderr,"http request failed: %s\n", err)
		} else {
			data, _ := ioutil.ReadAll(response.Body)
			var review Review
			if err := json.Unmarshal(data, &review); err != nil {
				fmt.Fprintf(os.Stderr,"json unmarshal failed: %s\n", err)
			} else {
				reviews = append(reviews, review)
			}
		}
	}
	return reviews
}

// magic starts here
func main(){

    // create a fixed array for the results
	results := make([][]Result, 5)

	// get all the game ids for the publisher
	game_ids := getGameIds()

	// get all the game data for those game ids
	games := getGames(game_ids)

	// get the reviews for each game, and manipulate some data
	for _, game := range games {
		reviews := getReviews(game)
		releaseDate := fmt.Sprintf("%d/%d/%d",
			game.Results.ExpectedReleaseMonth,
			game.Results.ExpectedReleaseDay,
			game.Results.ExpectedReleaseYear)
		for _, review := range reviews {
			result := Result{Title: game.Results.Name,
				Reviewer: review.Results.Reviewer,
				ReleaseDate: releaseDate}
			// append to an array of Results for each possible score.
			results[review.Results.Score - 1] =
				append(results[review.Results.Score - 1], result)
		}
	}

	// now that I've got the games/reviews in results arrays, 
	// build the output struct
	var output Output
	output.LabelName.ReviewsByScore.Key5 = results[4]
	output.LabelName.ReviewsByScore.Key4 = results[3]
	output.LabelName.ReviewsByScore.Key3 = results[2]
	output.LabelName.ReviewsByScore.Key2 = results[1]
	output.LabelName.ReviewsByScore.Key1 = results[0]

	// then, marshal the output struct into json and print it out
	if json, err := json.MarshalIndent(output, "", "    "); err != nil {
		fmt.Fprintf(os.Stderr,"json marshal failed: %s\n", err)
	} else {
		fmt.Println(string(json))
	}

}
